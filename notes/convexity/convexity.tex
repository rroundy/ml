
\documentclass[../notes.tex]{subfiles}
\graphicspath{{\subfix{figures/}}}

\begin{document}
\chapter{Convexity}

The point of this chapter is to provide the mathematical background and
theory in order to prove convergence of gradient descent and stochastic
gradient descent.  This chapter is going to be heavily technical.

\section{Linear and Affine spaces}

We are implicitly constructing spaces that are subsets of $\mathbb{R}^n$
for some $n$, or isomorphic enough to ignore any non-$\mathbb{R}^n$ness.

A {\bf linear} combination of two vectors $x$ and $y$ is the combination
$ax + by$, where $a,b$ are real numbers. A space $X$ is a {\bf linear
space} if every linear combination of vectors is also a member of the space.  If
we let $a, b$ take all values in $\mathbb{R}$ then the resultant set
is just the {\bf span}  (or linear-span) of those two vectors.
The linear combination of a collection of vectors $x_1, x_2, \ldots x_N$
is $\sum_{i=1}^N a_i x_i$ where each $a_i$ is a real number.  This
combination, as the $a_i$ take all values, is the {\bf span} of the $x_i$.
If we have some subset $S \subset \mathbb{R}^n$  the set of all linear
combinations of elements of $X$ is called the {\bf linear-hull} of $S$.

\begin{definition}
An {\bf affine} combination of two vectors $x$ and $y$ is the
combination $(1 - \lambda)x + \lambda y$ where $\lambda$ can take any
value in $\mathbb{R}$.  As $\lambda$ ranges over $\mathbb{R}$ this
describes a line in the space that passes through the points $x$ and
$y$.  An {\bf affine space}, $A$, is a space where if any two points $x$ and
$y$ are elements of $A$ so is all of their affine combinations.
More precisely, A space (or subset) $A$ is affine if $x, y \in A$
then $(1-\lambda)x +\lambda y \in A$ for all $\lambda \in \mathbb{R}$.
\end{definition}

Affine spaces need not contain the $0$ vector.  If an affine space does
contain the $0$ vector it is actually a linear space.
\begin{proof}
	Assume $A$ is affine and $0 \in A$.  Then for any $x,y$ in $A$
	\begin{equation}
		(1-\lambda)0 + \lambda x = \lambda x \in A,
	\end{equation}
	and
	\begin{equation}
		\left(1 - \frac{1}{2}\right)x + \frac{1}{2} y
		= \frac{1}{2}(x+y) \in A.
	\end{equation}
	The first equation says $A$ is closed under scalar
	multiplication, and the second says if $x,y \in A$ then
	$\frac{1}{2}(x+y) \in A$ combine that with the first equation
	shows that $x+y \in A$, which proves $A$ is close under vector
	addition.
\end{proof}


A set $T \subset \mathbb{R}^n$ is a {\bf translate} of a set,
$S \subset \mathbb{R}^n$, if there exits
some vector $a$ such that for any vector $x \in T$ there exists a vector
$b \in S$ which satisfies $x = b + a$.  More pictorially, a space $T$ is a
translate if you could rigidly move the space $S$ by a fixed vector
$a$ and end up with $T$.  We write symbolically $T = S + a$.
\begin{equation}
	T = S + a = \left\{ x + a : x \in S\right\}.
\end{equation}


A translate of a vector space is an affine space. Every affine space is a
translate of a unique vector space.  (See, e.g. Ref.~ \cite{convex-analysis}).
\begin{example}
	Given a vector $y \in \mathbb{R}^n$ and a $m\times n$ matrix $M$
	the set of solutions
	\begin{equation}
	A = \left\{x \in \mathbb{R}^n : Mx = y \right\}
	\end{equation}
	is an affine set.

	\begin{proof}
		Suppose $x_1$, and $x_2$ are solutions to the equation
		then
		\begin{eqnarray}
			M\left((1-\lambda)x_1 + \lambda x2\right)
			& = &(1-\lambda) Mx_1 + \lambda Mx_2 \\
			& = &(1-\lambda)y + \lambda y \\
			& = & y
		\end{eqnarray}
	\end{proof}
\end{example}
Moreover, every affine subset of $\mathbb{R}^n$ can be written this way.
We can also look at affine sets as intersections of hyperplanes. A
hyperplane is a $n-1$ dimensional surface in $\mathbb{R}^n$.  The
canonical representation of a hyperplane $H$ is
\begin{equation}
	H = \left\{ x : \left(n, x\right) = c \right\}.
\end{equation}
Where $(,)$ is the usual inner product, $n \in \mathbb{R}^n$ and $c \in \mathbb{R}$.
This is unique up to a common scaling of $n$ and $c$.  All hyperplanes
can be written in this way.  Every affine subset of $\mathbb{R}^n$ can
be written as the intersection of finite collection of hyperplanes,
see Ref.~\cite{convex-analysis}.

\begin{definition}
An {\bf affine combination} of the vectors $x_1, x_2, \ldots, x_N$ is the
sum $\sum_{i=1}^N \lambda_i x_i$ where $\sum_{i=1}^N \lambda_i = 1$.
\end{definition}
\begin{theorem}
A set $A$ is affine if and only if it contains all finite affine combinations
of its vectors.
\label{thm:affine-combinations}
\end{theorem}
\begin{proof}
The if direction is obvious because all affine combinations is clearly a
superset of pairwise affine combinations.  So suppose $A$ is affine.  We
will prove the claim by induction on number of vectors.  $A$ contains all
combinations of 1 and 2 vectors by definition of affine.  So suppose
$\sum_{i=1}^{N-1} \lambda_i x_i \in A$ if $\sum_{i=1}^{N-1} \lambda_i = 1$,
and $x_i \in A$ for all $i$.

Suppose we have a collection $x_1, x_2, \ldots, x_N \in A$, and
a set of coefficient $\lambda_1, \lambda_2, \ldots, \lambda_N$ with
$\sum_{i=1}^N \lambda_i = 1$.  We want to show
$\sum_{i=1}^N \lambda_i x_i \in A$.

Notice that $\sum_{i=1}^{N-1} +\lambda_N = 1$.  Look at
\begin{equation}
	\sum_{i=1}^N \lambda_i x_i =
	(1 - \lambda_N)\sum_{i=1}^{N-1} \frac{\lambda_i}{1 -
	\lambda_N}x_i + \lambda_N x_N.
\end{equation}
The term in the sum is a member of $A$  by our induction hypothesis, so
the entire expression is in $A$ by definition of an affine set.
\end{proof}

\begin{corollary}
	Let $A$ be an affine space and suppose
	$x, z, \left\{ x_i \right\} \in A$, and $
	\left\{\mu_i\right\} \in \mathbb{R}$ then
	\begin{equation}
		x + \sum_{i}\mu_i (x_i - z) \in A.
	\end{equation}
    This follows since the the coefficients of $x$ and the
    $\left\{x_i\right\}$ and $z$ sum to 1.  This says we stay in $A$ if
    we take a vector of $x \in A$ and add to it any {\bf linear
    combination} of vectors referenced to some common ``origin'' $z$.
    If we take $x$ and $z$ to be the same, we see we have a translate
    of a linear space.
\end{corollary}

\begin{example}
The affine combination of two (unequal) points describes a line (which
need not pass through the origin).

The affine-span of three
non-collinear points must describe a plane.  Specifically, if our points
are $x_1, x_2,$ and $x_3$ and our then the plane is
$(a,b) \mapsto ax_1 + bx_2 + (1-a-b)x_3$.  To see why this is a plane,
assume, for simplicity, that $x_1, x_2$, and $x_3 \in \mathbb{R}^3$.
Let $x \in \mathbb{R}^3$ be an arbitrary point  in the image of the map.
\begin{equation}
	x  = a x_1 + b x_2 + (1-a-b)x_3,
\end{equation}
for some real numbers $a$ and $b$.  We rewrite this as
\begin{equation}
    x - x_3 =  a (x_1 - x_3) + b (x_2 - x_3),
\end{equation}
to see that all points satisfy
\begin{equation}
	(x - x_3) \cdot \left( (x_1 - x_3) \times (x_2 - x_3) \right) = 0,
\end{equation}
which is the standard form for a plane $(n, x) = c$.

In the same way the affine relationship gives us a space of dimension
$n-1$ formed from the affine combination of $n$ linearly-independent
vectors.
\end{example}

\begin{definition}
This collection as the {\em free} $\lambda_i$ range over $\mathbb{R}$ is
called the {\bf affine-span} of the vectors.   If $S \subset
\mathbb{R}^n$, if we consider all the affine combinations of elements of
$S$ we have the {\bf affine-hull} of $S$, denoted
as $\aff S$.  This is the smallest affine
set containing $S$ or the intersection of all affine sets containing
$S$.
\end{definition}


The usual definition of interior and closure apply to affine spaces.
Let's introduce the unit ball and a notation from Ref.~\cite{convex-analysis}.
\begin{equation}
	B \stackrel{\text{def}}{=} \left\{ x : ||x|| \le 1 \right\}.
\end{equation}
Using this we can define the closed $\varepsilon$-ball around centered at
$y$ is written
\begin{eqnarray}
	B_{\varepsilon}(y) & = & y + \varepsilon B \\
	& = & \left\{ x : ||x - y|| \le
	\varepsilon \right\} \\
	 & = & \left\{ y + x : ||x|| \le \varepsilon \right \}.
\end{eqnarray}
Using this notation we define the interior and closure of a set $S$.
\begin{eqnarray}
	\interior S & = & \left\{ x : \exists \, \varepsilon > 0, x+ \varepsilon B
	\subset S \right\}, \\
	\closure S & = & \cap \left\{ S + \varepsilon B : \varepsilon > 0 \right\}.
\end{eqnarray}


\begin{definition}
	The {\bf relative interior}, written $\relint$, of a set $S \subset
	\mathbb{R}^n$, ($S$ is not necessarily affine) is
	\begin{equation}
		\relint S = \left\{ x \in \aff S :
		\exists \, \varepsilon > 0, (x + \varepsilon B) \cap \aff S
		\subset S
		\right\}.
	\end{equation}
	This is the interior where $S$ is regarded as a subset of its
	affine hull.

	Note that
	\begin{equation}
		\relint S \subset S \subset \closure S.
	\end{equation}
\end{definition}


\begin{definition}
    A map $f$ taking an affine space, $X$,  into an affine space $Y$, is
    a map such that whenever $\sum_i \lambda_i = 1$ we have
    \begin{equation}
        f\left( \sum_i \lambda_i x_i \right) = \sum_{i} \lambda_i f(x_i).
    \end{equation}
\end{definition}

An affine map between subspaces of $\mathbb{R}^n$ are categorized
by a linear map, $L$,  and a vector, $b$.  So a general affine map has the form
\begin{equation}
    x \mapsto Lx + b.
\end{equation}

\begin{proof}
    Suppose we have an affine map $f: X \rightarrow Y$.  Define a map
    $g : X \rightarrow Y$ by $g(x) = f(x) - f(0)$.  We will show the map
    $g$ is linear.

    \begin{enumerate}[label=(\roman*)]
        \item $g(ax) = ag(x)$.
            \begin{proof}
            Since $f$ is affine
            \begin{equation}
                \label{eqn:affine-simple}
                f(ax + (1-a)y) = af(x) +(1-a)f(y).
            \end{equation}
            Letting $y=0$ in Eqn.~(\ref{eqn:affine-simple}),
            \begin{eqnarray}
                f(ax) & = & a f(x) + (1-a)f(0), \\
                \left( f(ax) - f(0) \right) &= & a\left(f(x) - f(0) \right), \\
                g(ax) &=& a g(x).
            \end{eqnarray}
            \end{proof}

        \item $g(x+y) = g(x) + g(y)$.
            \begin{proof}
                Since $f$ is affine, we again start with
                Eqn.~(\ref{eqn:affine-simple}).  In this case we take
                $a=1/2$.
                \begin{eqnarray}
                    f\left(\frac{1}{2} x + \frac{1}{2} y\right) &=&
                    \frac{1}{2}f(x) + \frac{1}{2}f(y), \\
                    &=&
                    \frac{1}{2} \left(f(x) - f(0)\right) + \frac{1}{2}
                    \left(f(y) - f(0)\right) + f(0).
                \end{eqnarray}
                Subtract $f(0)$ from each side and rewrite in terms of
                $g$
                \begin{equation}
                    g\left( \frac{1}{2} \left( x  + y \right) \right)
                    = \frac{1}{2} g(x) + \frac{1}{2} g(y).
                \end{equation}
                Finally use result $i$, for scalar multiples to pull out
                the $1/2$.
                \begin{equation}
                    \frac{1}{2} g\left( x + y  \right)
                    = \frac{1}{2} \left( g(x) +  g(y) \right).
                \end{equation}
            \end{proof}
    \end{enumerate}

    This proves that every affine map is the combination of a linear map
    and a translation. A corollary of this is that every affine map is
    differentiable and the derivative is given by $Df = f(x) - f(0)$.
\end{proof}


\section{Convex Sets}

Convex sets are a subset of affine sets, so all the work above is
mimicked fairly closely.

\begin{definition}
	A {\bf convex combination} of points $x, y$ is the sum
	\begin{equation}
		(1 - \lambda)x + \lambda y,
	\end{equation}
	where $0 \le \lambda \le 1$.  This differs from the definition
	of an affine combination only in that we restrict the $\lambda$
	to the closed interval $[0,1]$.


	A {\bf convex set} $C \subset \mathbb{R}^n$ is a set where
	the convex combination of any two points is also an element of
	$C$. That is if $x, y \in C$ then $(1 - \lambda)x + \lambda y
	\in C$ for all $\lambda$ in $[0,1]$.


    A {\bf convex combination} of the $k$ vectors
    $\left\{x_1, x_2, \ldots, x_k \right\}$ is
    \begin{equation}
        \sum_{i=1}^{k} \lambda_i x_i, \text{ where }
        \sum_{i=1}^k~\lambda_i = 1 \text{ and }
        \lambda_i \ge 0, \text{ for each  } i.
    \end{equation}
\end{definition}


\begin{theorem}
	A set is convex if and only if it contains all finite convex
	combinations of its points.
\end{theorem}
\begin{proof}
	Analogous to the proof Theorem.~\ref{thm:affine-combinations}.
\end{proof}

\begin{example}
	The open and closed half-spaces are convex.
	For $x,b \in \mathbb{R}^n$ and $\alpha \in \mathbb{R}$,
	the sets
	$\left\{ x : \left(x, b\right) < \alpha \right\}$ and
	$\left\{ x : \left(x, b\right) > \alpha \right\}$ are called
	open half-spaces.  They are affine spaces and therefore convex.
	Similarly,
	$\left\{ x : \left(x, b\right) \le \alpha \right\}$ and
	$\left\{ x : \left(x, b\right) \ge \alpha \right\}$ are called
	the closed half-spaces.
\end{example}

A little thought shows that the arbitrary intersection of convex sets
must still be convex.


\begin{theorem}
	The image of a convex set under an affine map is convex.
\end{theorem}
\begin{proof}
    Suppose $f$ is an affine map from a convex set $X$ into a vector
    space $Y$.  We want to show that $\text{im} f \subset Y$ is convex.
    Let $z, w$ be any two points in the image of $f$, so there are
    points $x, y \in X$ such that $z = f(x)$ and $w = f(y)$.  But then,
    by convexity of $X$ the point $(1-\lambda)x + \lambda y$ is in $X$
    and since $f$ is affine
    $f((1-\lambda)x + \lambda y ) = (1-\lambda)f(x) + \lambda f(y)$ so
    the image of that point is on the line segment between $f(x)$  and
    $f(y)$ so that $\text{im} f$ is convex.
\end{proof}
\begin{corollary}
    The image of a convex set under a linear map is convex.
\end{corollary}

There are other functions and functional operation which preserve convex
sets.
\begin{example}
The perspective function $P : \mathbb{R}^{n+1} \rightarrow
\mathbb{R}^n$, with domain: $(x, t)$, $x \in \mathbb{R}^n$ and
$t \in \mathbb{R}$, restricted to $t>0$.
The perspective function is defined as
\begin{equation}
    P(x, t) = x/t.
\end{equation}
\end{example}

\begin{example}
    Linear-fractional functions.  These functions are created by taking
    the perspective function after an affine transformation.
    Let $P$ defined as in the last example and let $g : \mathbb{R}^n
    \rightarrow \mathbb{R}^{m+1}$ defined by
    \begin{equation}
        g(x) = \begin{pmatrix} A \\ c^{\text{T}} \end{pmatrix} x
             + \begin{pmatrix} b \\ d \end{pmatrix}.
    \end{equation}
    where $A \in \mathbb{R}^{m \times n}$, $b \in \mathbb{R}^m$, $c \in
    \mathbb{R}^n$ , and $d \in \mathbb{R}$. And we will demand
    that $c^\text{T}x +d > 0$. Then we define the
    linear-fractional function $f = P \circ g$.
    \begin{equation}
    f(x) = \frac{Ax + b}{c^\text{T} x + d}
    \end{equation}
\end{example}

\subsection{Simplices}
\subsection{Cones}


\section{Convex Functions}

In one dimension we have seen that when we are looking for a minimum, using
gradient descent we are not guaranteed to converge at all.  We have
seen that convergence may be slow.  We have seen that we can converge to
a local minimum, not a global.  Are there some set of requirements we can
put on our cost function to fix these problems and guarantee convergence?  The
answer is a qualified yes.

We begin with a set of necessary definitions.

{ \it
As is the case in real analysis, it's convenient to work with the
extended real numbers. We will consider functions that take either real
values or $\pm \infty$.  And adopt the usual conventions working with
$\infty$, where we need now to exercise care when subtraction or
division involving the symbols $\pm \infty$. } We denote this extended
interval $[-\infty, \infty]$.

\begin{definition}
	The {\bf epigraph} of a function
    $f : S \subset \mathbb{R}^n \rightarrow (-\infty, \infty]$,
	denoted $\epi f$, is the subset of
	$\mathbb{R}^{n+1}$ given by
	\begin{equation}
		\epi f = \left\{ (x, \mu) :  x \in S, \, \mu \in
		\mathbb{R}, \; \mu \ge f(x)\right\}.
	\end{equation}
\end{definition}

\begin{definition}
    A function $f : S \subset \mathbb{R}^n \rightarrow (-\infty, \infty]$
    is {\bf convex} if the set $\epi f$ is a convex set.

\begin{figure}[h]
	\includegraphics[height=3.5in]{epigraph.png}
	\caption{The shaded area is the epigraph of the function.}
	\label{fig:epigraph}
\end{figure}
\end{definition}
See figure 1.


It makes things easier if we extend the domain of all our functions to
all of $\mathbb{R}^n$ and any place our function is not defined define
it to be $\infty$.  Then the useful part of the domain is the
{\bf effective domain}, written $\domain f$, is defined as
\begin{equation}
\domain f = \left\{ x : x \in \mathbb{R}^n, f(x) < \infty \right\}.
\end{equation}



\begin{corollary}
    A function $f: \mathbb{R}^n \rightarrow (-\infty, \infty]$
    is convex if and only if for any $x, y$ in
    its domain, and any constants $\mu_x \ge f(x)$, $\mu_y \ge f(y)$
    and any $\lambda$ such that $0 \le \lambda \le 1$
	\begin{equation}
        f( (1-\lambda) x + \lambda y) \le (1-\lambda) \mu_x + \lambda \mu_y.
        \label{eqn:convex-fn1}
	\end{equation}
    \label{cor:convex-fn1}
\end{corollary}
\begin{proof}
    The set $\epi f$ is convex if whenever $(x, \mu_x)$, and
    $(y, \mu_y)$ are elements of $\epi f$ then so is the line segment
    \begin{equation*}
        (1 - \lambda)(x, \mu_x) +  \lambda(y, \mu_y)
        = \left((1-\lambda) x + \lambda y,
                (1-\lambda) \mu_x + \lambda \mu_y \right).
    \end{equation*}
    And that point is an element of $\epi f$ if
    \begin{equation*}
        f((1-\lambda)x + \lambda y) \le (1 - \lambda)\mu_x + \lambda \mu_y ,
    \end{equation*}

    Conversely, suppose that $f$ satisfies the inequality and let
    $(x, \mu_x)$, $(y, \mu_y)$ in $\epi f$, i.e. $\mu_x \ge f(x)$ and
    $\mu_y \ge f(y)$. Consider the point
    $(1-\lambda)(x , \mu_x) + \lambda(y, \mu_y)$.  We need to show it
    is an element of $\epi f$. But that is simply a restatement of
    Eqn.~(\ref{eqn:convex-fn1}).
\end{proof}
\begin{corollary}
    A function $f: \mathbb{R}^n \rightarrow (-\infty, \infty]$
    is convex if and only if for any $x, y$ in
	its domain and any $\lambda$ such that $0 \le \lambda \le 1$
	\begin{equation}
        f\left( (1-\lambda) x + \lambda y \right) \le (1-\lambda) f(x) +
		\lambda f(y).
	\end{equation}
	If the inequality is strict
	for $y \neq x$, we say $f$ is {\bf strictly convex}.
	We say a function $g$ is (strictly) {\bf concave} if $-g$ is
	(strictly) convex.
    \label{cor:convex-fn2}
\end{corollary}
\begin{proof}
    Now take $\mu_x  = f(x)$ and $\mu_y = f(y)$, because $(x, f(x))$,
    and $(y, f(y))$ are elements of $\epi f$ in Cor.~(\ref{cor:convex-fn1})
    \begin{equation*}
        f\left( (1-\lambda) x + \lambda y \right) \le
         (1- \lambda) f(x) + \lambda f(y).
    \end{equation*}

    Conversely, suppose $f$ satisfies the condition of the corollary.
    Then it's obvious that for any points $(x, \mu_x)$ and $(y, \mu_y)$
    in $\epi f$, i.e. $\mu_x \ge f(x)$ and $\mu_y \ge f(y)$ we have
    \begin{equation*}
        (1-\lambda)(x, \mu_x) + \lambda(y, \mu_y) =
        \left( (1-\lambda)x + \lambda y, (1-\lambda)\mu_x + \lambda
        \mu_y) \right).
    \end{equation*}
    By assumption
    \begin{equation}
        f( (1-\lambda)x + \lambda y) \le (1-\lambda)f(x) + \lambda f(y)
        \le (1-\lambda) \mu_x + \lambda \mu_y.
    \end{equation}
    Which shows the line segment is in $\epi f$ and $\epi f$ is convex.
\end{proof}

\begin{theorem}[Jensen's Inequality]
    Let $f$  be a function from $\mathbb{R}^n \rightarrow \left(
    -\infty, \infty \right]$.  Then $f$ is convex if and only if
    \begin{equation}
        f\left( \sum_{i=1}^k \lambda_i x_i \right)
        \le \sum_{i=1}^k \lambda_i f(x_i),
        \label{eqn:jensen}
    \end{equation}
    Whenever the collection $\left\{\lambda_i\right\}$ satisfies
    $\lambda_j \ge 0$ for all $j$ and $\sum_{i=1}^k \lambda_i = 1$.
\label{thm:jensen}
\end{theorem}
\begin{proof}
    Clearly, if a function satisfies Jensen's inequality
    it satisfies Cor.~(\ref{cor:convex-fn2}) and is therefore convex.

    The proof will proceed by induction on $k$.  The case for $k = 2$ is
    just Cor.~(\ref{cor:convex-fn2}).  So suppose the theorem is true
    for $k-1$. Let $x_i$ be $k$ points in $\mathbb{R}^n$, and
    $\lambda_i$  such that each is non-negative and the sum of all of
    them 1.  And assume wlog that $\lambda_k \neq 0$.

    Introduce $\lambda = \sum_{i=1}^{k-1} \lambda_i$ and $x =
    \sum_{i=1}^{k-1} \frac{\lambda_i}{\lambda} x^i$. Notice that for
    each $i$, $\lambda_i/ \lambda \ge 0$, and $\sum_{i=1}^{k-1}
    \lambda_{i}/\lambda = 1$, and $\lambda_k = 1 - \lambda$.
    Then we can write Eqn.~(\ref{eqn:jensen}) as
    \begin{equation}
        f\left( \sum_{i=1}^k \lambda_i x_i \right)
        = f\left(  \lambda x + (1-\lambda)x_k \right)
        \le \lambda f(x) + (1 - \lambda) f(x_k).
        \label{eqn:jensen-pf1}
    \end{equation}
    Where the inequality follows from $k=2$ case.
    We then write out $x$ and use the induction hypothesis.
    \begin{equation*}
        f(x) = f\left( \sum_{i=1}^{k-1}\frac{\lambda_i}{\lambda} x_i \right)
        \le \sum_{i=1}^{k-1} \frac{\lambda_i}{\lambda} f(x_i).
    \end{equation*}
    Putting this back into Eqn.~(\ref{eqn:jensen-pf1}) and eliminating
    $\lambda$ establishes the result.
\end{proof}

\subsection{Differential Criteria in One Dimension}
We now establish a few criteria that make deciding whether a function is
convex or not more simple when the function is more smooth.  We'll start with
the easiest case -- real valued functions of a single real variable.

\begin{lemma}
	A {\bf convex} function $f$ from $S \subseteq \mathbb{R}$ which
	is also differentiable satisfies the condition
	\begin{equation}
		f(y) \ge f(x) + (y - x) f'(x).
		\label{eqn:convex-diff-1d}
	\end{equation}
	Conversely, a differentiable function $f$ which satisfies
	Eqn.~(\ref{eqn:convex-diff-1d}) for all $x, y$ is convex.
    \label{lemma:convex-diff-1d}
\end{lemma}
\begin{proof}
	$(\Rightarrow)$
	Suppose $f$ is differentiable and convex. From the definition of
	convexity:
	\begin{equation}
		f(\lambda y + (1-\lambda)x) = f(x + \lambda(y -x)) \le
		\lambda f(y) + (1-\lambda)f(x),
	\end{equation}
	which we can arrange
	\begin{equation}
		\frac{f(x + \lambda(y -x) -f(x))}{\lambda} \le f(y) - f(x).
	\end{equation}
	Since, this is true for all $0 \le \lambda \le 1$, if we take
	the limit as $\lambda \rightarrow 0$.
	\begin{equation}
		f'(x)(y-x) \le f(y) - f(x).
	\end{equation}


	$(\Leftarrow)$ Suppose $f$ is differentiable and satisfies
	Eqn.~(\ref{eqn:convex-diff-1d}) for all $x, y$ in $S$.
	Let $x$ and $y$ be any two points in $S$, and let
	$\lambda \in [0,1]$.  Introduce $z = \lambda x + (1-\lambda) y$,
	since $z \in S$ (because $S$ is convex). Then we have
	\begin{eqnarray}
		f(x) & \le & f(z) + (x-z)f'(z) \\
		f(y) & \le & f(z) + (y-z)f'(z),
	\end{eqnarray}
	Multiply the first equation by $\lambda$, the second by
	$1-\lambda$ and add them up.  With some algebraic simplification
	and using the definition of $z$
	\begin{equation}
		\lambda f(x) + (1 - \lambda)f(y) \le f(z) = f(\lambda x
		+ (1-\lambda)y).
	\end{equation}
\end{proof}

\begin{lemma}
	A {\bf convex} function $f$ from $S \subseteq \mathbb{R}$ which
	is also twice differentiable satisfies the condition
	\begin{equation}
		f''(x) \ge 0.
		\label{eqn:convex-diff2-1d}
	\end{equation}
	Conversely, a twice-differentiable function $f$ which satisfies
	Eqn.~(\ref{eqn:convex-diff2-1d}) for all $x$ is convex.
\end{lemma}
\begin{proof}
	($\Rightarrow$) For any two points $x, y \in S$, assume wlog
	that $x < y$, by equation
	Eqn.~(\ref{eqn:convex-diff-1d}) we can write twice:
	\begin{eqnarray}
		f(y) & \ge & f(x) + (y - x) f'(x), \\
		f(x) & \ge & f(y) + (x - y) f'(y),
	\end{eqnarray}
	which we can arrange into the combined relation
	\begin{equation}
		f'(x)(y - x) \le f(y) - f(x) \le f'(y)(y -x).
	\end{equation}
	Thus,
	\begin{equation}
		\frac{f'(y) - f'(x)}{y - x} \ge 0.
	\end{equation}


	($\Leftarrow$) Suppose $f$ is twice differentiable on $S$ and
	that $f''(x) \ge 0$ for all $x \in S$. Let $x \le y \in S$
	arbitrary,  then by Taylor's theorem
	there is some $\xi \in [x, y]$ such that
	\begin{equation}
		f(y) = f(x) + f'(x)(y-x) + \frac{1}{2} f''(\xi)(y-x)^2.
	\end{equation}
	Since by supposition $f''(\xi) \ge 0$ we have
	\begin{equation}
		f(y) - f(x) - f'(x)(y-x) \ge 0.
	\end{equation}
	This implies the result by Lemma~(\ref{lemma:convex-diff-1d}).
\end{proof}

\section{Minima of Convex Functions}

We now establish uniqueness through convexity.  Suppose $f$ is strictly convex
and that we have two minimizers $x_0, y_0$,
$f(x_0) = f(y_0) \le f(x)$ for all $x\in S$.  Then by  strict convexity
$f\left(\frac{1}{2} x_0 + \frac{1}{2} y_0 \right) < \frac{1}{2}f(x_0) +
\frac{1}{2}f(y_0) = f(x_0)$.

Suppose now f is convex (not strictly convex) we have two local
minima $f(x_0) \neq f(y_0)$, and wlog assume $f(y_0) < f(x_0)$.  Since
$x_0$ is a local minima, there is a ball
of radius $r$ such that $f(x_0) \le f(x) \; \forall  \; x \in
B_r(x_0)$.  It's clear that $y_0 \notin B_r(x_0)$, otherwise either
would be the local minimizer. But there exists some $\lambda > 0$
such that $x' = \lambda x_0 + (1 -\lambda)y_0 \in B_r(x_0)$ and for
this $\lambda$ we have $f(x') \le \lambda f(x_0) + (1-\lambda)f(y_0)
\le f(x_0)$.


Therefore, if we can ensure our cost function is strictly convex we can
guarantee a unique minimizer.  Or, if our cost function is convex we can
guarantee that the cost of a local minimum is the cost of a global.
That is, we can't do better by chasing mimnima.



\section{Subgradients}

\section{Proof of Convergence- 1d}

If our function is {\bf convex}, differentiable, and Lipshitz
then we can prove that we converge to a minimum.  Convexity guarantees
that a local minimum is in fact global, for a convex function  the value
of the minimum is unique, while for a strictly convex function the
minimizer is unique too.







\begin{thebibliography}{999}
	\bibitem{hands-on}
		G\'eron, A. (2019),
		\emph{Hands-on Machine Learning with Scikit-Learn, Keras
		\& TensorFlow},
		O'Reilly Media Inc.
	\bibitem{understanding-machine-learning}
		Shalev-Shwartz, S. and Ben-David, S. (2014),
		\emph{Understaning Machine Learning},
		Cambridge University Press.

	\bibitem{nesterov-intro}
		Nesterov, Y. (2004),
		\emph{Introductory Lectures on Convex Optimization},
		Springer Science + Business Media.

	\bibitem{convex-analysis}
		Rockafellar, R. T. (1970),
		\emph{Convex Analysis},
		Princeton University Press.
	\bibitem{ml-opt}
		Aggarwall, C. (2020),
		\emph{Linear Algebra and Optimization for Machine Learning},
		Springer Nature Switzerland AG.
	\bibitem{convex-opt}
		Boyd, S. and Vindenberghe, L. (2004)
		\emph{Convex Optimization},
		Cambridge University Press.
\end{thebibliography}
\end{document}
