import argparse
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import seaborn

seaborn.set_theme()
seaborn.set_style('darkgrid')

#plt.rcParams.update({'text.usetex': True})

xs = np.arange(-2, 2, 0.01)
f = lambda x : (x*x - 1)**2 + (x-1)
ys = np.vectorize(f)(xs)

def make_epigraph():
    fig, ax = plt.subplots()
    ax.plot(xs, ys, color='blue')
#    ax.text(0, 4, 'epi $f$')
    ax.fill_between(xs, ys, 10.0, color='blue', alpha=0.1)

def main():
    this_file = pathlib.Path(__file__)

    parser = argparse.ArgumentParser(this_file.stem)
    parser.add_argument('-o', '--output', help='name of file to output',
            default=None)

    args = parser.parse_args()

    make_epigraph()

    if args.output:
        plt.savefig(args.output, format='png')
    else:
        plt.show()

if __name__ == '__main__':
    main()
