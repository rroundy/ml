\documentclass[../notes.tex]{subfiles}
\graphicspath{{\subfix{figures/}}}

\begin{document}
\chapter{Gradient Descent}

\section{motivation}
Recall that we have a cost function, $L$,  for which we are trying to
adjust our parameters $\boldsymbol{\omega}$\footnote{
	Remember we've homogenized $\boldsymbol{\omega}$  and
	$\mathbf{x}$ by incorporating the intercept term as $\omega_0$ and
	forcing $x_0 = 1$.  },
to find the minimal value.  For regular least squares the loss function
is just the sum of the squares of the residuals.
\begin{equation}
	L_{\text{ls}}(\boldsymbol{\omega}) = \frac{1}{N} \sum_{i=1}^N \left(
	y_i  - \mathbf{x}_i \cdot \boldsymbol{\omega} \right)^2.
	\label{eqn:ls-def}
\end{equation}
The sub-index $i$ in Eqn. (\ref{eqn:ls-def}) refer to our training set,
with $\mathbf{x}_i$ the vector of features for the $i$-th entry and
$y_i$ the expected result.

Another loss function, that we have considered is the hinge-loss
function.  This was the default loss function for sk-learn SGD solver.
\begin{equation}
	L_{\text{hinge}}(\boldsymbol{\omega}) = \frac{1}{N} \sum_{i=1}^N
	\text{max}\left(0, 1 - y_i\mathbf{x}_i \cdot \boldsymbol{\omega}
	\right),
	\label{eqn:hl-def}
\end{equation}
The hinge loss function introduces a couple of new complications.  The
$y_i$ only take the values $0$ or $1$.  And the function is not
differentiable everywhere.


There are a few things to notice about our loss functions, the first is
that they take the form of a sum of individual functions (one for each
training set).
\begin{equation}
	L(\boldsymbol{\omega}) = \sum_{i=1}^N L_i(\boldsymbol{\omega}),
\end{equation}
where for least squares
\begin{equation}
	L_{\text{ls},i}(\boldsymbol{\omega}) =
	\frac{1}{N}\left( y_i - \mathbf{x}_i \cdot \boldsymbol{\omega}
\right)^2,
\end{equation}
and similarly
\begin{equation}
	L_{\text{hinge}, i}(\boldsymbol{\omega}) =
	\frac{1}{N}\text{max}(0, 1 - y_i\mathbf{x}_i \cdot
	\boldsymbol{\omega}).
\end{equation}
This observation will serve as our jump off point for {\bf stochastic}
gradient descent.

So we have a cost function and we want to find the minimum.  In some
cases there is an analytic solution, but in most cases there isn't a
closed form solution for the minimum.  Even when there is an explicit
solution for the minimum, it is often vastly more computationally
expensive to actually calculate it.

In lieu of actually calculating minima, we will look for approximation
schemes which get us {\bf close} to the minima, but are much easier to
calculate.


\section{gradient descent -- idea and algorithm}

The idea for finding minima is usually the one everyone learns in basic
calculus.  Take the derivative, set it equal to zero and solve.  The
reason, we recall, is that for smooth functions local maxima and minima
always occur where the derivative is $0$.  There are complications, of
course, in $1$-d we have inflection points where the derivative is $0$,
but the point is neither a local maximum or local minimum.  In higher
dimensions whe have saddle points, which are minima along one direction
but maxima or inflection points along others.

\begin{figure}[h]
	\includegraphics[height=3.5in]{one_d.eps}
	\caption{1-d descent. At $p_1$ the slope is negative so we
	should go to the right to find the minimum.  And similarly, at
	$p_2$ the slope is positive so we should look to the left.  }
	\label{fig:one_d}
\end{figure}

Let's start with the simplest scenario -- a function in $1$-d for which
we want to find the minimum.  In $1$-d the minimum is either to the left
or right of the point we're looking at.  If the slope is positive it's
probably to the left and if the slope is negative we should go right,
see Fig.~(\ref{fig:one_d}).  Further, if the slope is bigger we should
move more.  But, we should also give ourself some wiggle room.  So let's
introduce a scaling parameter for each step $\eta$.  Putting this all
together gives us the basic structure of gradient descent
Fig.~(\ref{fig:gd1d}).
\begin{figure}[H]
	Gradient Descent algorithm in 1-d
	\begin{algorithmic}[1]
		\State \textbf{given} initial position $x_0$, and fudge factor $\eta$
		\While {not good enough}{}
			\State $x_{i+1} := x_{i} - \eta f'\left(x_i\right)$
		\EndWhile
		\State \Return $x_{\text{best}}$
	\end{algorithmic}
	\caption{gradient descent in 1-d}
	\label{fig:gd1d}
\end{figure}
Note we've deliberately glossed over ``good enough'' and $x_\text{best}$
in the algorithm Fig.~(\ref{fig:gd1d}).


\subsection{What can go wrong?}

There are a few things that can go wrong with our algorithm
Fig.~(\ref{fig:gd1d}).  There are two broad classes of problems that we
want to focus on.  The learning rate $\eta$, if we pick it badly we can
not converge on a solution, or converge on a solution too slowly.

Too see this happen, consider the code in Fig.~(\ref{fig:gdconverge}).
\begin{figure}[h]
\inputminted[linenos]{python}{\subfix{listings/gdconverge.py}}
\caption{\texttt{list\_grad\_descent} implements the algorithm
	Fig.~(\ref{fig:gd1d}) but keeps a list of all steps along the
	way. The choice for being done is just iterate a fixed number of
	times.
	}
\label{fig:gdconverge}
\end{figure}
\begin{figure}
	\includegraphics[scale=0.6]{convergence.eps}
	\caption{
		Here we see 20 steps of convergence for various $\eta$
		plotted from the code in Fig.~(\ref{fig:gdconverge}).
		The starting $x_0$ is taken to be $-2$.  The function we
		are looking at is $f(x) = x^2 - 1$.  In the bottom most
		plot the point $x_i$ alternates between $\pm 2$, never
		converging.  For larger $\eta$  the solution runs away
		to infinity.  }
	\label{fig:convergence}
\end{figure}

The output of \texttt{list\_grad\_descent}  for a few choices of $\eta$
are plotted in Fig.~(\ref{fig:convergence}).  They show good
convergence, slow convergence and non-convergence respectively.  The
learning parameter $\eta$ (or the learning schedule $\eta_i$) is
something we need to enforce from outside the gradient descent algorithm
itself. There exist techniques for picking good values, we will address
those techniques in other notes.


The function $f$ to be minimized can also affect whether and how
convergence happens. Consider the function $f(x) = (x^2 -1)^2 +x -1$.
\begin{figure}
	\includegraphics[scale=0.5]{nonconvex_convergence.eps}
	\caption{
		Here we see 20 steps of convergence for various $x_0, \eta$
		combinations for the function $(x^2 -1)^2 + x-1$. We see
		slow convergence, jumping global
		minimum in favor of local minima, and good solutions.
		}
	\label{fig:nonconvex_convergence}
\end{figure}
This function has the property of having local and global minima.  The
gradient descent algorithm doesn't distinguish and just settles on a
{\bf local} minimum. There exist techniques to try to overcome these
shortcomings, such as simulated annealing and momentum methods.  We'll
touch on these in later notes.

\section{Analytic properties}

{\em The ``natural'' framework for solvable optimization problems are convex
functions which are Lipshitz-continuous or Lipshitz-smooth.  Convexity
helps to guarantee that minima are actually achievable and Lipshitz
guarantees the derivative doesn't change too much, which will be
necessary for estimating convergence.  Right now, we can consider
these to be technical details that we will use in the proof of
convergence, but without a real need to focus on. }

\section{Gradient Descent in $\mathbb{R}^m$ }

There is actually no substantive difference between the 1-d and many-d
version of the algorithm.  The basic framework is to pick the learning
schedule $\eta_i$, which is picked ahead of time.  Pick some termination
condition like a predetermined number of steps, $N$, or a smallness parameter
$\epsilon$, so that you stop when $x_{i+1}~-~x_{i} < \epsilon$ or $i = N$.
A sketch of the algorithm is given in Fig.~(\ref{fig:gradient_descent}).
\begin{figure}[H]
	Gradient Descent algorithm
	\begin{algorithmic}[1]
		\State \textbf{given} initial position $x_0$, learning
		        schedule $\eta_i$, step cutoff $N$, and
			termination condition $\epsilon > 0$.
		\While {$i < N$}{}
			\State $x_{i+1} := x_{i} - \eta_i \nabla f (x_i)$
			\If {$x_{i+1} -x_{i} < \epsilon$}
			 \State \Return $x_{i+1}$
			 \EndIf
		\EndWhile
		\State \Return $x_{N}$
	\end{algorithmic}
	\caption{gradient descent}
	\label{fig:gradient_descent}
\end{figure}

\section{Subgradient Descent}

The first thing we would like to relax is strict differentiability.
Notice is that the cost function Eqn.~(\ref{eqn:hl-def}) is not actually
differentiable at $x=1$, see Fig.~(\ref{fig:hlcost}).
\begin{figure}[hb]
	\includegraphics[scale=0.4]{hlcost.eps}
	\caption{hinge-loss cost function. }
	\label{fig:hlcost}
\end{figure}
A {\bf convex} function admits a generalization of the gradient, called
the {\bf subgradient}.  The details will be given in the chapter on
Convexity.  For completeness we state the definition of a subgradient for the function
$f$ (assumed convex), at a point $x$.
\begin{definition}
	A vector $v$ is called a subgradient of $f$ at $x$ if for all
	$y$ in the domain of $f$
	\begin{equation}
		f(y) \ge f(x) + \left(y - x, v\right).
	\end{equation}
	\label{def:subgradient}
	Where $\left(,\right)$ is the usual inner product.  The set of
	all subgradients at $x$ is called the {\em differential set} and
	denoted $\partial f(x)$.
\end{definition}
We should note that for a differentiable convex function the
differential set contains only the single element
$\partial f(x) = \left\{ \nabla f(x) \right\}$.
At each point where our convex cost function is not differentiable, we will
substitute a subgradient vector from the differential set, then the
algorithm proceeds exactly as Fig.~(\ref{fig:gradient_descent}).


Since subgradient descent is mostly just a ``fix-up'' of gradient
descent we'll just call everything gradient descent, and if our function
is not differentiable, we'll just implicitly use a subgradient at each
point of non-differentiability.


\section{Variations}
\subsection{variations of step size}
The simplest thing to pick for $\eta_i$ is to set $\eta_i = \eta$ for
all $i$.  This is the easiest step-choice to analyze, but isn't
necessarily the best choice for convergence.  If your function is
guaranteed to be convex - this isn't a bad choice.

Another up front choice is $\eta_i = \frac{\eta}{\sqrt{i+1}}$.  This
choice sometimes shows up in the literature. It's not particularly used
in practice.

A more complex example is that we set
\begin{equation}
	\eta_i = \argmin_{\alpha\ge 0} f(x_i - \alpha \nabla f(x_i)).
\end{equation}
This says, at every step take our step size to be the value that's going
to minimize the expression on the right.  This is called {\em line
search}.

There exists more complex line search, e.g. {\em backtracking line
search}.


\subsection{variations of step procedure}

As a consequence of proving convergence, we often want to establish that
our initial guess $x_0$ is close enough to our optimal point.  In
particular we may be able to limit our domain to some subset $S' \subset
S$, and if our step ever takes us outside of $S'$ we want to project
back into $S'$.

The projection will work something like
\begin{equation}
	x' = \pi_{S'}(x) = \argmin_{y \in S'} || x - y ||.
\end{equation}
That is, find the closest point to $x$ that lies in $S'$.

In this case we modify the gradient descent algorithm
Fig.~(\ref{fig:gradient_descent}), by replacing line $3$ with
\begin{figure}[H]
	\begin{algorithmic}[1]
		\State $y_{i} := x_{i} - \eta_i \nabla f (x_i)$
		\State $x_{i+1} := \pi_{S'}(y_i)$.
	\end{algorithmic}
\end{figure}


\section{Stochastic Gradient Descent}

Calculating the full gradient at every step still turns out to be very
costly if the number of training points is large.  That is, even if we
have a closed form solution to every $L_i$ we still have to evaluate $N$
function calls at every step.


Instead of evaluating the gradient over all $L_i$ each time, we pick a
training data set {\bf randomly} (and fairly) from the set $\left\{1, 2,
\ldots, N\right\}$. Suppose our cost function $L$ can be written as
\begin{equation}
	L(x) = \sum_{a=1}^N L_a(x; z_a).
\end{equation}
In this I've replaced what was $\omega$ with $x$ to tie it into the
exposition for gradient descent.  What were our training data are now
called $z_a$. Our $x \in \mathbb{R}^m$ is a vector representing each of
our features.
\begin{figure}[H]
	Stochastic Gradient Descent algorithm
	\begin{algorithmic}[1]
		\State \textbf{given} initial position $x_0$, learning
		        schedule $\eta_i$, step cutoff $N$.
		\While {$i < N$}{}
			\State select $a$ randomly from
			$\left\{1, 2, \ldots, N\right\}$.
			\State $x_{i+1} := x_{i} - \eta_i \nabla L_a (x_i)$
		\EndWhile
		\State \Return $x_{N}$
	\end{algorithmic}
	\caption{stochastic gradient descent}
	\label{fig:stochastic_gradient_descent}
\end{figure}

This algorithm converges {\bf on average}, in the sense, that the
expectation of $x_i$ wrt the probability distribution for $a$ (uniform,
here) converges just like the usual gradient descent algorithm.


The algorithm as put in Fig.~(\ref{fig:stochastic_gradient_descent})
bounces around too much.  So we can damp out the oscillations in time
by adjusting the learning schedule. This is what is used in
Ref.~\cite{hands-on} pp 125.  For completeness that algorithm is presented in
Fig.~(\ref{fig:damped_stochastic_gradient_descent}).
vectors.
\begin{figure}[H]
	damped Stochastic Gradient Descent algorithm
	\begin{algorithmic}[1]
		\State \textbf{given} initial position $x_0$, step
		cutoff $N$, and two convergence parameters $t_0$ and
		$t_1$.
		\For{$i = 0, 1, \ldots, N-1$} {}
			\For{$j=0, 1, \ldots, m-1$}{}
			\State select $a$ randomly from $\left\{ 1, 2, \ldots, N \right\}$
			\State define $t := mi +j$
			\State $\eta_{t} := \frac{t_0}{t + t_1}$
			\State $x_{t+1} := x_{t} - \eta_{t}\nabla L_a(x_{t})$
			\EndFor
		\EndFor
	\end{algorithmic}
	\caption{damped stochastic gradient descent, Ref.~\cite{hands-on}}
	\label{fig:damped_stochastic_gradient_descent}
\end{figure}

\subsection{Mini-batch Gradient Descent}


\begin{thebibliography}{999}
	\bibitem{hands-on}
		G\'eron, A. (2019),
		\emph{Hands-on Machine Learning with Scikit-Learn, Keras
		\& TensorFlow},
		O'Reilly Media Inc.
	\bibitem{understanding-machine-learning}
		Shalev-Shwartz, S. and Ben-David, S. (2014),
		\emph{Understaning Machine Learning},
		Cambridge University Press.
	\bibitem{nesterov-intro}
		Nesterov, Y. (2004),
		\emph{Introductory Lectures on Convex Optimization},
		Springer Science + Business Media.
\end{thebibliography}
\end{document}
