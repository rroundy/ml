def list_grad_descent(grad, init_x, eta, npoints):
    xlist = np.ndarray((npoints, ))
    x = init_x
    for i in range(npoints):
        xlist[i] = x
        x = x - eta*grad(x)
    return xlist
