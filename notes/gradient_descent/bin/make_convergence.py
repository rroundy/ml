import argparse
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import seaborn

seaborn.set_theme()
seaborn.set_style('darkgrid')

xi = np.arange(-2, 2, 0.01)

f = lambda x: x**2 - 1
df = lambda x: 2.0*x

def list_grad_descent(grad, init_x, eta, npoints):
    xlist = np.ndarray((npoints, ))
    x = init_x
    for i in range(npoints):
        xlist[i] = x
        x = x - eta*grad(x)
    return xlist

def draw_descent(ax, starting_x, stepsize, npoints, label_n=-1):
    gradpts = list_grad_descent(df, starting_x, stepsize, npoints)
    pts = list(zip(gradpts, f(gradpts)))

    ax.plot(xi, np.vectorize(f)(xi), label='$f$')
    y_max = np.max([f(-2), f(2)])
    ax.set_ylim(-1.5, y_max + 0.1)
    ax.set_xlim(-2, 2)
    for i, (x,y) in enumerate(pts):
        ax.plot(x, y, 'ro')

        if (label_n >0) and (i < label_n):
            ax.text(x + 0.1, y, f'{i+1}')

def main():
    this_file = pathlib.Path(__file__)

    parser = argparse.ArgumentParser(this_file.stem)
    parser.add_argument('-o', '--output', help='name of file to output',
            default=None)

    args = parser.parse_args()

    fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, figsize=(6,8))
    ax1.set_title('$\eta=0.1$')
    draw_descent(ax1, -2, 0.1, 20, 5)
    ax2.set_title('$\eta=0.01$')
    draw_descent(ax2, -2, 0.01, 20, 1)
    ax3.set_title('$\eta=1$')
    draw_descent(ax3, -2, 1, 20, 1)

    if args.output:
        plt.savefig(args.output, format='eps', dpi=600)
    else:
        plt.show()

if __name__ == '__main__':
    main()
