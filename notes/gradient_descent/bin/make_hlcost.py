import matplotlib.pyplot as plt
import argparse
import pathlib
import seaborn
import numpy as np

seaborn.set_theme()
seaborn.set_style('darkgrid')

def main():
    this_file = pathlib.Path(__file__)
    parser = argparse.ArgumentParser(this_file.stem)
    parser.add_argument('-o', '--output', help='name of file to output',
            default=None)

    args = parser.parse_args()

    xi = np.arange(-2, 4, 0.01)
    f = lambda x: np.max([0.0, 1.0-x])
    plt.plot(xi, np.vectorize(f)(xi))

    if args.output:
        plt.savefig(args.output, format='eps')
    else:
        plt.show()

if __name__ == '__main__':
    main()

