import numpy as np
import matplotlib.pyplot as plt
import seaborn
import pathlib
import argparse

seaborn.set_theme(style='darkgrid')
plt.rcParams.update({'text.usetex': True})

f = lambda x: x**2 - 1.0
df = lambda x: 2.0 * x

def mk_tangent(f, df, x, L=1.0):
    m = df(x)
    y = f(x)

    geom = np.sqrt(1+m*m)
    xmin = x - 0.5*L/geom
    xmax = x + 0.5*L/geom
    xs = np.arange(xmin, xmax, (xmax - xmin)/100)
    ys = y + m*(xs - x)
    return xs, ys

def main():
    this_file = pathlib.Path(__file__)

    parser = argparse.ArgumentParser(this_file.stem)
    parser.add_argument('-o', '--output', help='name of file to output',
            default=None)

    args = parser.parse_args()

    t1 = mk_tangent(f, df, -1.5, L=2.0)
    t2 = mk_tangent(f, df, .75, L=2.0)

    xi = np.arange(-2, 2, 4/100)
    plt.plot(xi, np.vectorize(f)(xi))
    plt.plot(-1.5, f(-1.5), 'ro')
    plt.plot(t1[0], t1[1], 'r-')
    plt.plot(0.75, f(0.75), 'go')
    plt.plot(t2[0], t2[1], 'g-')
    plt.text(-1.5 + 0.1, f(-1.5), '$p_1$')
    plt.text(0.75 - 0.2, f(0.75), '$p_2$')

    if args.output:
        plt.savefig(args.output, format='eps')
    else:
        plt.show()

if __name__ == '__main__':
    main()
