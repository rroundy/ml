import argparse
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import seaborn
import sys

seaborn.set_theme()
seaborn.set_style('darkgrid')

xi = np.arange(-3, 3, 0.01)

f = lambda x: (x**2 -1)**2 + x - 1
df = lambda x: 2.0*(x**2 -1)*(2.0*x) + 1.0

def list_grad_descent(grad, init_x, eta, npoints):
    xlist = np.ndarray((npoints, ))
    x = init_x
    for i in range(npoints):
        xlist[i] = x
        x = x - eta*grad(x)
    return xlist

def draw_descent(ax, starting_x, stepsize, npoints, label_list=None):
    gradpts = list_grad_descent(df, starting_x, stepsize, npoints)
    pts = list(zip(gradpts, f(gradpts)))

    ax.plot(xi, np.vectorize(f)(xi), label='$f$')
    y_max = 12
    ax.set_ylim(-3, y_max + 0.1)
    ax.set_xlim(-2.5, 2.5)
    for i, (x,y) in enumerate(pts):
        ax.plot(x, y, 'ro')
        if label_list is not None and i in label_list:
            ax.text(x, y+1, f'{i+1}')

def main():
    this_file = pathlib.Path(__file__)

    parser = argparse.ArgumentParser(this_file.stem)
    parser.add_argument('-o', '--output', help='name of file to output',
            default=None)
    parser.add_argument('-l', '--list', help='make list of xi\'s',
            action='store_true', default=False)

    args = parser.parse_args()

    fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8)) = plt.subplots(
            4, 2, sharex=True, figsize=(12,12))

    ax1.set_title('$x_0=-2, \eta=0.1$')
    draw_descent(ax1, -2, 0.1, 20, [0, 1, 19])
    ax2.set_title('$x_0=-2, \eta=0.05$')
    draw_descent(ax2, -2, 0.05, 20, [0, 19])

    ax3.set_title('$x_0=0, \eta=0.1$')
    draw_descent(ax3, 0, 0.1, 20, [0, 19])
    ax4.set_title('$x_0=0, \eta=0.05$')
    draw_descent(ax4, 0, 0.05, 20, [0, 19])

    ax5.set_title('$x_0=0.75, \eta=0.1$')
    draw_descent(ax5, 0.75, 0.1, 20, [19])
    ax6.set_title('$x_0=0.75, \eta=0.05$')
    draw_descent(ax6, 0.75, 0.05, 20, [19])

    ax7.set_title('$x_0=2, \eta=0.1$')
    draw_descent(ax7, 2, 0.1, 20, [0, 19])
    ax8.set_title('$x_0=2, \eta=0.05$')
    draw_descent(ax8, 2, 0.05, 20, [0, 19])

    if args.list:
        print(list_grad_descent(df, -2, 1, 20))
        sys.exit(0)

    if args.output:
        plt.savefig(args.output, format='eps', dpi=600)
    else:
        plt.show()


if __name__ == '__main__':
    main()
