\documentclass[../notes.tex]{subfiles}
\graphicspath{{\subfix{figures/}}}

\begin{document}
\chapter{Linear Regression (Least Squares)}

One of the main aspects of machine learning seems to be linear
regression.  So we'll take some time to set up the basic problem and
methods for solving it.  There are some other forms of linear regression
that we will eventually need to look at, but for these notes we use
linear regression to mean least squares.

\begin{figure}[h]
  \includegraphics[scale=0.75]{linear_fit.eps}
  \caption{Least squares}
  \label{fig1}
\end{figure}

\section{Basic setup}

Given a set of points in $\mathbb{R}^2$, $\left\{ (y_1, x_1), (y_2, x_2),
\ldots, (y_n, x_n) \right\}$ find the line which ``best fits'' the points.

To put this on mathematical footing, we actually set this up as a
minimization problem. Assume we have a line $y=mx +b$, We define the
residual for the point $i$ by $e_i = y_i - ( m x_i + b)$.  And we pick
$m$ and $b$ to minimize the sum of the squares of the residuals:
\begin{equation}
  \sum_{i=1}^{n} ||e_i||^2
  = \sum_{i=1}^n || y_i - m x_i -b ||^2.
\end{equation}


This is a smooth function with a global minimum.  To find the minimum,
we take the derivative wrt $m$, and $b$, set them equal to zero and
solve.
\begin{eqnarray}
  \sum_{i=1}^n \left( -2 x_i \right)\left(y_i - m x_i - b \right) = 0 \\
  \sum_{i=1}^n \left( -2 \right)\left(y_i - m x_i - b \right) = 0
\end{eqnarray}
Solving this system for $m$ gives:
\begin{eqnarray}
  m & = & \frac{n \sum_{i=1}^n x_i y_i - \sum_{i=1}^n x_i \sum_{j=1}^n y_j}{
    n \sum_{i=1}^n x_i^2 - \sum_{i=1}^n x_i \sum_{j=1}^n x_j } \\
    & = & \frac{\left< xy \right> - \left<x\right> \left<y\right>}{
     \left< x^2 \right> - \left< x\right>^2}.
\end{eqnarray}
Where we have introduced the sample averages:
\begin{equation}
  \left< f \right> = \frac{1}{n} \sum_{i=1}^n f_i.
\end{equation}
We can write the equation for the intercept $b$ as
\begin{eqnarray}
  b & = & \frac{-\sum_{i=1}^n x_i \sum_{j=1}^n x_j y_j +
  \sum_{i=1}^n x_i^2 \sum_{j=1}^n y_j}{
    n \sum_{i=1}^n x_i^2 - \sum_{i=1}^n x_i \sum_{j=1}^n x_j } \\
    & = & \frac{\left< y \right> \left< x^2 \right> -
    \left<x\right> \left<xy\right>}{
      \left< x^2 \right> - \left< x\right>^2} \\
    & = & \left< y \right> - m \left< x \right>.
\end{eqnarray}


\section{Higher Dimensional Space}

The next generalization is allowing
$x_i \rightarrow \vec{x}_i \in \mathbb{R}^m $ to have $m$ values.  The
inputs now come in as $\left\{ (y_1, \vec{x}_1), (y_2, \vec{x}_2),
\ldots  (y_n, \vec{x}_n)\right\}$. Instead of fitting the single
parameter ($m$ - the slope), we have $m+1$
coefficients $\omega_1, \omega_2, \ldots, \omega_m$, and $b$.

The residuals are defined as $e_i = y_i - ( \sum_j \omega_j x_{i,j} + b)$.
If we introduce the column vector $\vec{\omega}$ we can write the
function to minimize as
\begin{equation}
  \sum_{i=1}^n || y_i - \vec{\omega} \cdot \vec{x}_i -b ||^2.
\end{equation}
There are a few tricks to make this easier to handle.  Extend the vectors
$\vec{x}$ in  $\mathbb{R}^m$, to the vector $(1, \vec{x})$ in
$\mathbb{R}^{m+1}$.  Simultaneously, we extend $\vec{\omega}$ to
$(b, \vec{\omega})$ in $\mathbb{R}^{m+1}$.  For both these it is
convenient to start counting from $0$.  The cost function is then
written
\begin{equation}
  F(\vec{\omega}) = \sum_{i=1}^n|| y_i - \vec{\omega}\cdot \vec{x}_i ||^2.
\end{equation}

We can take the $\vec{x}_i$ and put them together in an $n \times m$
matrix, identify $\vec{\omega}$ with the column vector
\begin{equation}
  X = \begin{pmatrix} \text{---} & \vec{x}_1 & \text{---} \\
      \text{---} & \vec{x}_2 & \text{---} \\
               & \vdots & \\
      \text{---} & \vec{x}_n & \text{---}
  \end{pmatrix}, \quad
  y = \begin{pmatrix} y_1 \\ y_2 \\ \vdots \\ y_n \end{pmatrix},
    \quad
  \omega = \begin{pmatrix} \omega_0 \\ \omega_1 \\ \vdots \\
  \omega_{m} \end{pmatrix}.
\end{equation}
The cost function takes the form of the matrix expression:
\begin{equation}
  F(\omega) = \left( y - X \omega \right)^{T} \left( y - X \omega \right).
\end{equation}
Note that this equation is also a trace expression:
\begin{eqnarray}
  F(\omega) & = & \text{tr} \left( \left( y - X \omega \right)^{T}
  \left( y - X \omega \right) \right) \\
   & = & \text{tr}\left( \omega^T X^T X \omega - 2 y^T X \omega +
   y^T y \right).
\end{eqnarray}
Writing these equations in the trace form allows us to take the
derivatives slightly more easily.

\subsection{Matrix Derivatives.}

In this section we briefly derive some results  for taking the
derivative wrt the components of a matrix.
Let's define our function as
\begin{equation}
  f(X) = \text{tr}\left( A X \right),
\end{equation}
where $A$ is a constant $n \times m$ matrix.  We define the derivative
component-wise, if $X$ is a $m \times n$ matrix we define the derivative
as the $m \times n$ matrix given by
\begin{equation}
  \left[ \frac{\partial f(X)}{\partial X} \right]_{ij} =
  \frac{\partial f(X)}{\partial X_{ij}}.
\end{equation}

{\it
As an aside this definition isn't universal.  Usually when defining the
derivative wrt a (column) vector we get a (row) vector, the gradient.
}

For the trace expression we have
\begin{equation}
  f(X) = \sum_{i=1}^{n} \left( AX \right)_{ii}
       = \sum_{i=1}^{n} \sum_{j=1}^m A_{ij} X_{ji}.
\end{equation}
Which gives us, from,
\begin{equation}
  \frac{\partial f(X)}{\partial{X_{ij}}}
       = \sum_{k=1}^{n} \sum_{l=1}^m A_{kl} \frac{\partial X_{lk}}{\partial X^{ij}}
       = \sum_{k=1}^{n} \sum_{l=1}^m A_{kl} \delta_{li} \delta_{kj}
       = A_{ji}
       = A^T_{ij}.
\end{equation}
the simple result
\begin{equation}
  \frac{\partial \text{tr}(AX)}{\partial X} = A^T.
\end{equation}


\subsection{A slight generalization.}
The next step more complicated is the generic quadratic (in $X$) trace
expression.  Suppose we define our $f$ to be
\begin{eqnarray}
  f(X) & = & \text{tr}\left(A X B X^T C \right) =
  \sum_{klmno} A_{kl} X_{lm}  B_{mn} X^T_{no} C_{ok} \\
   & = &
   \sum_{klmno} A_{kl} X_{lm}  B_{mn} X_{on} C_{ok}.
\end{eqnarray}
If we take the derivative with respect to $X$ we'll get two terms
\begin{eqnarray}
  \frac{\partial f(X)}{\partial X_{ij}}
  & = & \sum_{klmno}\left(
  A_{kl} \delta_{li} \delta_{mj} B_{mn} X_{on} C_{ok} +
  A_{kl} X_{lm} B_{mn} \delta_{oi} \delta_{nj} C_{ok}
  \right) \\
  & = &
  \sum_{kno}\left( A_{ki} B_{jn} X_{on} C_{ok} \right) +
  \sum_{klm}\left( A_{kl} X_{lm} B_{mj} C_{ik} \right) \\
  & = &
  \sum_{kno}\left( A^T_{ik} C^T_{ko} X_{on} B^T_{nj} \right) +
  \sum_{klm}\left( C_{ik} A_{kl} X_{lm} B_{mj} \right) \\
  & = &
  \left( A^T C^T X B^T \right)_{ij} +
  \left( C A X B \right)_{ij}.
\end{eqnarray}
Which we can summarize this as
\begin{equation}
  \frac{\partial \text{tr}\left( A X B X^T C \right)}{\partial X}
   = A^T C^T X B^T + C A X B.
\end{equation}

\subsection{Trace tricks.}
We can use the cyclic property of the trace and the result of a constant
matrix to derive the results, using the usual rules of calculus.
The fact that the trace of a matrix is the same as the trace of the
transpose, $\text{tr}(A) =\text{tr}(A^T)$, gives us
\begin{equation}
  \frac{\partial \text{tr} \left( AX^T \right)}{\partial X}
  = \frac{\partial \text{tr} \left( X A^T\right)}{\partial X}
  = \frac{\partial \text{tr} \left( A^T X \right)}{\partial X}
  = \left(A^T \right)^T = A.
\end{equation}

Then we can do the more complicated one by treating each $X$ as constant
while arranging the other into the standard form
\begin{equation}
  \frac{\partial \text{tr}\left( A X|_{\text{const}} B X^T C \right)}{\partial X}
  = \frac{\partial \text{tr}\left( C A X|_{\text{const}} B X^T\right)}{\partial X}
  = C A X|_{\text{const}} B.
\end{equation}
And similarly
\begin{equation}
  \frac{\partial \text{tr}\left( A X B X|_{\text{const}}^T C \right)}{\partial X}
  = \frac{\partial \text{tr}\left(  B X|_{\text{const}}^T C A X \right)}{\partial X}
  = \left( B X|_{\text{const}}^T C A \right)^T.
\end{equation}
The final result comes from adding these two terms together.


\section{The normal equation.}

Using the results of the previous section, for least squares we get:
\begin{eqnarray}
  \frac{\partial F(\omega)}{\partial \omega} & = &
  \frac{\partial}{\partial \omega}
  \text{tr}\left(
     \omega^T X^T X \omega - 2 y^T X \omega + y^T y
  \right) \\
  & = & 2 X^T X \omega - 2 X^T y.
\end{eqnarray}
Setting this expression equal to zero and solving gives us the {\bf normal
equation} for the solution:
\begin{equation}
  \omega = \left(X^T X\right)^{-1}X^Ty.
\end{equation}

\section{Regularization.}

In order to overcome certain problems with our first formulation -- maybe
the matrix $X^TX$ is non-invertable, maybe  we're picking up too many
features in the fits, or maybe our variance is too large,
we change the cost function by adding a regularization term.  This
change tends to increase {\bf bias} and decrease {\bf variance}.


\subsection{Motivation.}
The matrix $X^T X$ is not guaranteed to be invertible.  One condition
that is equivalent to invertibility is that the matrix is positive definite.
A generic symmetric matrix is positive semi-definite.

A trick that can be used to invert a matrix is to add a small positive
perturbation.  This idea, if followed, leads directly to the Moore-Penrose
inverse, and is also called Tikhonov regularization.

We add to $X^T X$ the small correction $\lambda \text{I}$, where
$\lambda > 0$.  The matrix $\left(X^T X  + \lambda \text{I}\right)$ is always
invertible.  We can look at the limit of the expression
\begin{equation} \label{tikhonov}
	\omega = \lim_{\lambda \rightarrow 0^+} \left(X^T X + \lambda
	\text{I}\right)^{-1}X^T y.
\end{equation}

It's also possible to keep the $\lambda$ as a fixed parameter called a
{\bf meta parameter} to our optimization problem.

\subsection{Ridge regularization.}
Instead of solving our original optimization problem we add the extra
term quadratic in $\omega$
\begin{eqnarray}
	F_{\text{ridge}}(\omega) & = &
	\left(y - X\omega\right)^T\left(y - X\omega\right) + \lambda
	\omega^T \omega  \\
	& = &
	\left|\left|y - X\omega\right|\right|^2_2
	+ \lambda \left|\left|\omega\right|\right|^2_2.
\end{eqnarray}
This form of cost function has an explicit solution, which is the same
as equation (\ref{tikhonov}), without the limit procedure.
\begin{equation}
	\omega_\lambda = \left(X^T X + \lambda \text{I} \right)^{-1}X^Ty.
\end{equation}

\subsection{Lasso regularization.}

Instead of adding the $\ell_2$-norm as the cost we use the $\ell_1$-norm.
\begin{eqnarray}
	F_{\text{Lasso}}(\omega) & = &
	\left(y - X\omega\right)^T\left(y - X\omega\right) + \lambda
	||\omega||_1\\
	& = &
	\left|\left|y - X\omega\right|\right|^2_2 + \lambda
	||\omega||_1.
\end{eqnarray}
The $\ell_1$-norm of a vector is defined
\begin{equation}
	||\omega||_1 = \sum_{i} \left|\omega_i\right|.
\end{equation}.

Unfortunately, this form is not explicitly differentiable, so other
methods are employed to actually find the minimum.

\subsection{Elastic net regularization.}

The idea here is to add a combination of Ridge and Lasso terms
\begin{eqnarray}
	F_{\text{Elastic net}}(\omega) & = &
	\left|\left|y - X\omega\right|\right|^2_2
	+ \alpha ||\omega||_1
	+ (1 - \alpha) || \omega ||_2^2.
\end{eqnarray}


\end{document}
