import argparse
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import seaborn

seaborn.set_theme()
seaborn.set_style('darkgrid')

plt.rcParams.update({'text.usetex': True})

seaborn.set_theme()
seaborn.set_style('darkgrid')

def main():
    this_file = pathlib.Path(__file__)

    parser = argparse.ArgumentParser(this_file.stem)
    parser.add_argument('-o', '--output', help='name of file to output',
            default=None)
    args = parser.parse_args()

    np.random.seed(67)
    xn = np.random.random(17) * 5.0
    yn_actual = 3.0 * xn + 1.0
    noise = np.random.normal(0, 3.5, xn.shape[0])
    yn = yn_actual + noise

    m, b = np.polyfit(xn, yn, 1)

    pos=13
    ei_x = xn[pos] - 0.18
    ei_y = 0.5 *(yn[pos] + m *xn[pos] + b)

    plt.cla()
    plt.plot(xn, yn, 'or', label='data')
    plt.plot(xn, yn_actual, ':k', label='true line')
    plt.plot(xn, m*xn +b, '-g', label='best guess')
    plt.plot((xn[pos], xn[pos]), (yn[pos], m*xn[pos] + b), '-b')
    plt.text(ei_x, ei_y, '$e_i$')
    plt.text(xn[pos] + 0.1, yn[pos], '$(x_i, y_i)$')
    plt.ylim(-6, 21)
    plt.legend()

    if args.output:
        plt.savefig(args.output, format='eps')
    else:
        plt.show()

if __name__ == '__main__':
    main()
