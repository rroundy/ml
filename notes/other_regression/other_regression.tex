\documentclass[../notes.tex]{subfiles}
\graphicspath{{\subfix{figures/}}}

\begin{document}
\chapter{Logistic and Softmax Regression}

We can use the technology of our linear regression, our convex
optimization to work out different classifiers.  We start with
a binary classifier using {\em Logistic regression}.  We then generalize
that to a multi-classifying system {\em Softmax regression}.



\section{Logistic Regression}

The idea behind logistic regression is to separate points that belong to
the class and points that don't belong to the class using a smooth
transformation of a planar boundary.  Specifically, if we're give the
data $x$ (a vector in feature space), and a vector of weights $\omega$.
We'll get to training $\omega$ in a second. We define the probability
that $x$ is in our class as
\begin{equation}
    p(x, \omega) = \frac{1}{1 + \exp\left(-x^{T} \omega \right)}.
    \label{eqn:logistic_probability}
\end{equation}
The logistic function, Eqn.~(\ref{eqn:logistic_def}),
\begin{equation}
    \sigma(t) = \frac{1}{1 + \exp\left(-t\right)},
    \label{eqn:logistic_def}
\end{equation}
maps the real line into the range $\left[ 0, 1 \right]$, with the point
$t=0$ mapping to $1/2$.  So the output of $\sigma$ makes sense as a
probability.  Our prediction then is
\begin{equation}
    y = \left\{ \begin{matrix} 1 \text{ if } \sigma \ge 1/2 \\
                               0 \text{ if } \sigma < 1/2
    \end{matrix} \right..
\end{equation}
The decision point for classification for $t$ is $t=0$.  Geometrically,
this is the point where $- x^T \omega = 0$, i.e.,
we put all the values in one class on one side of the hyperplane
$x^T \omega = 0$.

\subsection{Training the Model}

We need a function such that whenever $y$ is $1$ we have $x^T \omega$
with one sign and vice versa.  The cost function that is used for that
is
\begin{equation}
    C(x, y, \omega) = -y \log p(x, \omega) - (1 - y) \log \left( 1- p(x,
    \omega) \right).
\end{equation}
In this $y$ is $0$ if $x$ is not in our class and $1$ if $x$ is in the
class. Since $y$ only takes the values of $0$ and $1$ only one term in
the cost function is activate at a time.  If $y = 1$ we end up trying to
minimize $-\log p$ which pushes $p$ away from $0$ and toward $1$.
Conversely if $y = 0$ we are trying to minimize $-\log(1- p)$, which in
this case means we want $p$ small.

We will need to calculate the derivative for gradient descent.  So, we
do it now.
\begin{equation}
    \frac{\partial C}{\partial \omega_i}
     = \left( \frac{-y}{p} + \frac{ 1 - y}{1 -p} \right) \frac{\partial
     p}{\partial \omega_i}
      = \left( \frac{p - y}{p(1-p)} \right) \frac{\partial p}{\partial
      \omega_i}.
\end{equation}
We can calculate the derivative of $p$ easily enough,
\begin{equation}
    \frac{\partial}{\partial \omega_i} \frac{1}{1 + \exp\left(-x^T
    \omega\right) }
     = -\frac{1}{\left( 1+ \exp\left(-x^T \omega \right) \right)^2}
     \exp(-x^T \omega) (-x_i).
\end{equation}
This is simplified if we notice this is (using $t$ for $x^T \omega$)
\begin{equation}
    \frac{\partial p}{\partial \omega_i} =
    \frac{1}{1 + \exp(-t)} \cdot \frac{\exp(-t)}{1 + \exp(-t)} x_i
    = p(1-p) x_i.
\end{equation}
And so the derivative of our cost function simplifies to
\begin{equation}
    \frac{\partial C}{\partial \omega_i} = (p(x, \omega) - y)x_i.
    \label{eqn:logistic_grad_single}
\end{equation}


Now we can calculate for all our training data.  We define the cost
function as
\begin{equation}
    J(\omega) = \frac{-1}{N} \sum_{i=1}^N \left(y^{(i)} \log p^{(i)}
    +  (1 - y^{(i)}) \log \left(1 - p^{(i)}\right)
    \right),
\end{equation}
where the superscript labels the instance of training data, explicitly,
\begin{equation}
    p^{(i)} = \frac{1}{1 + \exp\left(-\left.x^{(i)}\right.^T \omega
    \right)}.
\end{equation}

We can then differentiate term by term using
Eqn.~(\ref{eqn:logistic_grad_single}) to calculate our gradient for
gradient descent.
\begin{equation}
    \nabla_\omega J(\omega) = \frac{1}{N} \sum_{i=1}^N \left( p^{(i)} - y^{(i)}
    \right) x^{(i)}.
\end{equation}








\section{Softmax Regression}

Softmax is a generalization of Logistic regression which can directly
handle multiple classes. Each class $k = 1, 2, \ldots K$ will get its
own parameter vector $\omega^k$. Where the dimension of $\omega^k$ is
the number of features. Notice we'll use the superscript here for the
index over classes, it doesn't denote raising to a power.

For a given set of features $x$ we define a score for that class,
\begin{equation}
    s^k(x) = s^k(x, \omega^k) = x^T \omega^k = \sum_{i=1}^{M} x_i \omega^k_i.
\end{equation}
Where $M$ is the number of features in the data. Each vector $x$ is
assigned $K$-scores, one corresponding to each class.  We define the
probability that our data has class $k$ as
\begin{equation}
    p^k = \frac{\exp\left(s^k(x)\right) }{
        \sum_{j=1}^K \exp\left(s^j(x)\right) }.
\end{equation}
Our prediction for softmax is
\begin{equation}
    y_{\text{predicted}} = \operatorname*{argmax}_k \left( s^k(x) \right)
    = \operatorname*{argmax}_k
    \left(x^T \omega^k \right).
\end{equation}

We can write this in terms of the matrix $\Omega$ which has $\omega^k$
as the $k$-th row, called the {\em parameter matrix}.
\begin{equation}
    \Omega =
    \begin{pmatrix}
        \rule[.5ex]{3.5em}{0.4pt} \, \omega^{(1)} \, \rule[.5ex]{3.5em}{0.4pt} \\
        \rule[.5ex]{3.5em}{0.4pt} \, \omega^{(2)} \, \rule[.5ex]{3.5em}{0.4pt} \\
        \vdots \\
        \rule[.5ex]{3.5em}{0.4pt} \, \omega^{(K)} \, \rule[.5ex]{3.5em}{0.4pt} \\
   \end{pmatrix}.
\end{equation}
Then $\omega^{(k)}$ is $\Omega^T e_k$ where $e_k$ is the standard unit
vector consisting of all 0s and a 1 in the $k$-th row. So that
$s^k = x^T \omega^{(k)}  = x^T \Omega^T e_k = e_k^T \Omega x$.

\subsection{Training the Model}

We use the {\bf cross entropy cost function} to train our data.

\subsubsection{Entropy}

Entropy is a concept stolen from physics and applied to information
theory by Shannon.  Given a probability distribution $\left\{p_i\right\}$ we
define the entropy $H$ of the distribution to be
\begin{equation}
    H(\left\{p_i \right\}) = -\sum_i p_i \log p_i .
\end{equation}
The entropy is, roughly, a measure of the disorder of the distribution.
Or the amount of ``surprise'' there is in the values.  Imagine if the
results were certain, all the $p$'s are zero except for one which is
unity.  Then the Entropy is 0.  There is no uncertainty -- or we won't
be surprised by the outcome.  Conversely suppose we wish to maximize the
entropy $H$, allowing ourselves to modify the values of $p_i$, and that
the indexing set (from which $i$ takes its vales is finite -- say $N$
points in the sample space).
\begin{equation}
    \left.
    \left. \max_{p_i} \right| _{i=1 \ldots N}\left( -\sum_{i=1}^N p_i
    \log p_i \right) \middle/ \sum_{i=1}^N p_i = 1
    \right.
\end{equation}
The easiest way to maximize this is the method of Lagrange multipliers.
( Another way is to notice the symmetry, that the $p_i$ enter all the
equations symmetrically, so their values ought to all be the same. )
An elementary exercise in constrained optimization  tells us that all the
$p_i$'s should be equal and should be $1/N$.  Since the second
derivative of the function is negative for all $p_i \in [0,1]$ it is
concave and its maximum is unique.  That is when we measure our random
variable we have no information to tell us which result will come up
more frequently, i.e. we have maximum ``surprise''.


\subsubsection{Cross Entropy}
Given two probability distributions defined over the same space $S$,
let's call them $\left\{p_i\right\}$ and $\left\{q_j\right\}$.  We
define the {\bf cross entropy} as
\begin{equation}
    H(p, q) = - \sum_{i} p_i \log q_i.
\end{equation}

TODO: important properties of cross entropy.


\subsubsection{Cross Entropy Cost Function}
The training data are defined by the two vectors $x$ and $y$ where $x$ has
dimensions of features and $y$ had dimension of number of classes.
The $y$ are typically all 0s except a 1 in the class which applies.
Rigorously, we will demand that each $y_i \ge 0$ and $\sum_{i=1}^K y_i =
1$, so that we can interpret each entry as the probability that $x$ is
an example of class $i$.

The cost for the training pair $(x, y)$ is given by the cross entropy
function with $y_i$ being the $p$-s and $p^{(i)}$ being the $q$-s.
\begin{eqnarray}
    C(x, y, \Omega) & = & - \sum_{i=1}^K y_i \log p^{i}(x) \\
     & = & - \sum_{i=1}^K y_i \log \frac{\exp s^{i}(x) }{ \sum_{j=1}^K
     \exp s^{j}(x)}.
    \label{eqn:softmax_cross_cost}
\end{eqnarray}







\end{document}
